﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace RIAT_LABS
{
    //todo: на каждый вызов метода создается класс  XmlSerializer, DataContractJsonSerializer, надо сделать так чтобы не было лишних выделений памяти
    public class XMLSerialize : ISerialize
    {
        public T Deserialize<T>(string someSerialize)
        {
            var serializer = new XmlSerializer(typeof(T));
            var stream = new MemoryStream();

            var sw = new StreamWriter(stream);
            stream.Position = 0;
            sw.WriteLine(someSerialize);
            sw.Flush();
            stream.Position = 0;
            var reader = XmlReader.Create(stream);
            return (T)serializer.Deserialize(reader);
        }

        public bool Satisfy(DataType type)
        {
            return type == DataType.Xml;
        }

        public string Serialize<T>(T output)
        {
            var serializer = new XmlSerializer(typeof(T));
            var stream = new MemoryStream();
            var xmlsettings = new XmlWriterSettings() { OmitXmlDeclaration = true };
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            var writer = XmlWriter.Create(stream, xmlsettings);
            serializer.Serialize(writer, output, ns);
            using (var sr = new StreamReader(stream))
            {
                stream.Position = 0;
                return sr.ReadToEnd();
            }
        }
    }
}
