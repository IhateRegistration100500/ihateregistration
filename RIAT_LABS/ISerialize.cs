﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIAT_LABS
{
    public interface ISerialize
    {
        bool Satisfy(DataType type);

        string Serialize<T>(T output);
        T Deserialize<T>(string someSerialize);
    }
}
