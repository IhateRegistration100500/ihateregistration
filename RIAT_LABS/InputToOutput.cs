﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIAT_LABS
{
    public class InputToOutput
    {
        public static Output MakeOutputFromInput(Input someInput)
        {
            var Res = new Output();
            Res.SumResult = someInput.Sums.Sum() * someInput.K;
            Res.MulResult = someInput.Muls.Aggregate((acc, i) => acc * i);
            Res.SortedInputs = someInput.Sums.Concat(someInput.Muls.Select(i => (decimal)i)).ToArray();
            Res.SortedInputs.OrderBy(x => x);
            Res.SortedInputs = Res.SortedInputs.Select(i => i + 0.0M).ToArray();//костыль ибо бакс
            return Res;
        }
    }
}
