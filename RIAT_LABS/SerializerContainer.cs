﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RIAT_LABS
{
    class SerializerContainer
    {
        private readonly List<ISerialize> serializers = new List<ISerialize>
        {
            new JSONSerialize(),
            new XMLSerialize()
        };

        public ISerialize GetSerializer(DataType type)
        {
            return serializers.First(s => s.Satisfy(type));
        }
    }
}
