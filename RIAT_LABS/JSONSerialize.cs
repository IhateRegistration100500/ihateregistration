﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;

namespace RIAT_LABS
{
    public class JSONSerialize : ISerialize
    {

        public T Deserialize<T>(string someSerialize)
        {

            var stream = new MemoryStream();
            var serializer = new DataContractJsonSerializer(typeof(T));
            var sw = new StreamWriter(stream);
            sw.Write(someSerialize);
            sw.Flush();
            stream.Position = 0;
            return (T)serializer.ReadObject(stream);
        }

        public bool Satisfy(DataType type)
        {
            return type == DataType.Json;
        }

        public string Serialize<T>(T output)
        {
            var stream = new MemoryStream();
            var jss = new DataContractJsonSerializerSettings();
            var serializer = new DataContractJsonSerializer(typeof(T));
            serializer.WriteObject(stream, output);
            stream.Position = 0;
            using (var sr = new StreamReader(stream))
            {
                return sr.ReadToEnd();
            }
        }
    }
}
