﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace RIAT_LABS
{

    class Program
    {

        static void Main(string[] args)
        {
            var serializeType = Console.ReadLine();

            //var type = (DataType)Enum.Parse(typeof(DataType), serializeType, true);
            if (Enum.IsDefined(typeof(DataType), serializeType))
            {
                var type = (DataType)Enum.Parse(typeof(DataType), serializeType, true);
                var container = new SerializerContainer();
                var serializer = container.GetSerializer(type);
                var someText = Console.ReadLine();
                if (!String.IsNullOrEmpty(someText))
                {
                    var li = serializer.Deserialize<Input>(someText);
                    var output = InputToOutput.MakeOutputFromInput(li);
                    Console.WriteLine(serializer.Serialize<Output>(output));
                }
            }
        }



    }
}
